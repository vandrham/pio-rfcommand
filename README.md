# pio-rfcommand

platformio.ini:

  * lib_deps = git@bitbucket.org:vandrham/pio-rfcommand.git

```
#include <RFCommand.h>

RFCommand rfCommand(PIN_NUMBER);

void send()
{
    // code: bitwise code set with dip switches on controller
	// button: one of A, B, C, D or E
	// value: true for switching on, false for switching off
	rfCommand.send(10, 'B', true);
}
```
