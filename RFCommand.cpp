#include "RFCommand.h"
#include <RemoteTransmitter.h>

RFCommand::RFCommand(byte pin)
{
	static ActionTransmitter actionTransmitter(pin);
	transmitter = &actionTransmitter;
}

void RFCommand::send(int code, char button, bool value)
{
	transmitter -> sendSignal(code, button, value);
}