#ifndef rfcommand_h
#define rfcommand_h

#include <RemoteTransmitter.h>

class RFCommand
{
    public:
        RFCommand(byte pin);
        void send(int code, char button, bool value);
    private:
        ActionTransmitter *transmitter;
};

#endif